# Bibliography databases

This repository contains my bibliography databases in BIBTeX format.
They are split by main topic.

The entries contain a `language` field used by **babel** as well as
`url` and `isbn` fields supported by **natbib**.

These files should reside in `bibtex/bib` in a local texmf tree. A
reasonable cloning strategy on a Unix-like system would therefore be:

```
$ git clone https://gitlab.com/vigou3/bibliography.git ~/texmf/bibtex/bib
```

The database `vg.bib` is probably of no interest for people other than
myself. ;-)
